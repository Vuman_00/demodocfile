/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demodocfile;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author vuman_000
 */
public class Demodocfile {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        BufferedReader reader = null;
        ArrayList<Meo> list = new ArrayList<>();
        try {
            reader = new BufferedReader(new FileReader("meo.txt"));
            while (true) {                
                String dong =(String ) reader.readLine();
                if(dong==null) break;
        
              String[] meo = dong.split(",");          
              Meo meomem = new Meo(Integer.valueOf(meo[0]),Integer.valueOf(meo[1]),meo[2],Integer.valueOf(meo[3]));       
              list.add(meomem);         
                
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Demodocfile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Demodocfile.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                reader.close();
            } catch (IOException ex) {
                Logger.getLogger(Demodocfile.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
           for (Meo meo : list) {
                 System.out.println(meo.toString());
            
        }
     
        
   
}
}
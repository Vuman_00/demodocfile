/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demodocfile;

/**
 *
 * @author vuman_000
 */
public class Meo {
    
    private int id;
    private int idParent;
    private String ten;
    private  int tuoi;

    @Override
    public String toString() {
        return "Meo{" + "id=" + id + ", idParent=" + idParent + ", ten=" + ten + ", tuoi=" + tuoi + '}';
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdParent() {
        return idParent;
    }

    public void setIdParent(int idParent) {
        this.idParent = idParent;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public int getTuoi() {
        return tuoi;
    }

    public void setTuoi(int tuoi) {
        this.tuoi = tuoi;
    }

    public Meo() {
    }

    public Meo(int id, int idParent, String ten, int tuoi) {
        this.id = id;
        this.idParent = idParent;
        this.ten = ten;
        this.tuoi = tuoi;
    }
    
    
}
